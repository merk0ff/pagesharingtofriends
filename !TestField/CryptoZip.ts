// Crypto library
import * as crypto from "./Blowfish";
import {Blowfish} from "./Blowfish";

// CryptoZip class
export class cryptoZip {
    private key : string;
    private dataCompressor : Blowfish;

    constructor(key: string) {
        this.key = key;
        this.dataCompressor = new Blowfish(key);
        console.log("Successfully launched CryptoZip class");
    }
    /*
     * Decompress and than decrypt function
     */
    public DeCZ(data: string) : string {
        let decr : string;

        try {
           decr = this.dataCompressor.decrypt(data);
        }
        catch (e) {
            console.log("Sorry we fucked up " + e);
            return null;
        }

        return decr;
    }

    /*
     * Compress and than crypt function
     */
    public CZ(data: string) : string {
        let cr : string;

        try {
            cr = this.dataCompressor.encrypt(data);
        }
        catch (e) {
            console.log("Sorry we fucked up " + e);
            return null;
        }

        return cr;
    }
}